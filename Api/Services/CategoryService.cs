using System.Collections.Generic;
using Api.DTO;
using Api.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace Api.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IMongoCollection<Category> _categories;

        public CategoryService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("StoreDb"));
            var database = client.GetDatabase("Store");
            _categories = database.GetCollection<Category>("Categories");
        }

        public List<Category> Get()
        {
            return _categories.Find(category => true).ToList();
        }

        public Category Get(string id)
        {
            return _categories.Find<Category>(category => category.Id == id).FirstOrDefault();
        }

        public Category Create(CategoryDto category)
        {
            var newCategory = new Category{
                Id = category.Id,
                Name = category.Name,
            };

            _categories.InsertOne(newCategory);
            return newCategory;
        }

        public void Update(string id, CategoryDto categoryIn)
        {
            var newCategory = new Category{
                Id = id,
                Name = categoryIn.Name,
            };

            _categories.ReplaceOne(category => category.Id == id, newCategory);
        }

        public void Remove(Category categoryIn)
        {
            _categories.DeleteOne(category => category.Id == categoryIn.Id);
        }

        public void Remove(string id)
        {
            _categories.DeleteOne(category => category.Id == id);
        }
        
    }
}