using System.Collections.Generic;
using Api.DTO;
using Api.Entities;

namespace Api.Services
{
    public interface IProductService
    {
        List<Product> Get();
        Product Get(string id);
        Product Create(ProductDto category);
        Product Update(string id, ProductDto categoryIn);
        void Remove(Product categoryIn);
        void Remove(string id);
    }
}