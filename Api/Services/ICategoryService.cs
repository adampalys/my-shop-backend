using System.Collections.Generic;
using Api.DTO;
using Api.Entities;

namespace Api.Services
{
    public interface ICategoryService
    {
        List<Category> Get();
        Category Get(string id);
        Category Create(CategoryDto category);
        void Update(string id, CategoryDto categoryIn);
        void Remove(Category categoryIn);
        void Remove(string id);
    }
}