using System.Collections.Generic;
using System.Linq;
using Api.DTO;
using Api.Entities;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Api.Services
{
    public class ProductService : IProductService
    {
        private readonly IMongoCollection<Product> _product;

        public ProductService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("StoreDb"));
            var database = client.GetDatabase("Store");
            _product = database.GetCollection<Product>("Products");
        }

        public List<Product> Get()
        {
            return _product.Find(product => true).ToList();
        }

        public Product Get(string id)
        {
            return _product.Find<Product>(product => product.Id == id).FirstOrDefault();
        }

        public Product Create(ProductDto product)
        {
            var newProduct = new Product{
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                Categories = product.Categories.Select(x => new BsonObjectId(ObjectId.Parse(x)))
            };

            _product.InsertOne(newProduct);
            return newProduct;
        }

        public Product Update(string id, ProductDto productIn)
        {
            var newProduct = new Product{
                Id = productIn.Id,
                Name = productIn.Name,
                Description = productIn.Description,
                Price = productIn.Price,
                Categories = productIn.Categories.Select(x => new BsonObjectId(ObjectId.Parse(x)))
            };

            _product.ReplaceOne(product => product.Id == id, newProduct);

            return newProduct;
        }

        public void Remove(Product productIn)
        {
            _product.DeleteOne(product => product.Id == productIn.Id);
        }

        public void Remove(string id)
        {
            _product.DeleteOne(product => product.Id == id);
        }
    }
}